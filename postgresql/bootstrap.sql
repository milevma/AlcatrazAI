CREATE OR REPLACE FUNCTION foreign_keys () RETURNS json IMMUTABLE LANGUAGE SQL AS $$
  SELECT COALESCE(Array_to_json(Array_agg(Row_to_json(row))), '[]')
  FROM   (
    SELECT o.conname AS CONSTRAINT_NAME,
       m.relname AS TABLE_NAME,
  (SELECT a.attname
   FROM pg_attribute a
   WHERE a.attrelid = m.oid
     AND a.attnum = o.conkey[1]
     AND a.attisdropped = FALSE) AS COLUMN_NAME,
       f.relname AS foreign_table,
  (SELECT a.attname
   FROM pg_attribute a
   WHERE a.attrelid = f.oid
     AND a.attnum = o.confkey[1]
     AND a.attisdropped = FALSE) AS foreign_column
FROM pg_constraint o
LEFT JOIN pg_class c ON c.oid = o.conrelid
LEFT JOIN pg_class f ON f.oid = o.confrelid
LEFT JOIN pg_class m ON m.oid = o.conrelid
WHERE o.contype = 'f'
  AND o.conrelid IN
    (SELECT oid
     FROM pg_class c
     WHERE c.relkind = 'r')
    ) row;
  $$ ;

CREATE OR REPLACE FUNCTION primary_keys() RETURNS json IMMUTABLE LANGUAGE SQL AS $$
  SELECT COALESCE(Array_to_json(Array_agg(Row_to_json(row))), '[]')
  FROM (
    SELECT t.table_name AS TABLE, array_agg(c.column_name::text) AS PRIMARY_KEYS
    FROM information_schema.key_column_usage AS c LEFT JOIN information_schema.table_constraints AS t ON t.constraint_name = c.constraint_name
    WHERE t.constraint_type = 'PRIMARY KEY'
    GROUP BY t.table_name
    ORDER BY t.table_name) row;
$$;

CREATE ROLE readuser;
GRANT CONNECT ON DATABASE app_db TO readuser;
GRANT USAGE ON SCHEMA public TO readuser;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO readuser;

CREATE ROLE edituser;
GRANT CONNECT ON DATABASE app_db TO edituser;
GRANT USAGE ON SCHEMA public TO edituser;
GRANT SELECT ON ALL TABLES IN SCHEMA public to edituser;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public to edituser;
GRANT EXECUTE ON FUNCTION primary_keys() to edituser; 
GRANT EXECUTE ON FUNCTION foreign_keys() to edituser; 

DROP TABLE IF EXISTS change_log;
CREATE TABLE change_log (
p_id SERIAL PRIMARY KEY,
change_timestamp timestamp NOT NULL,
table_changed TEXT NOT NULL,
primary_key_of_changed_row TEXT NOT NULL,
column_changed TEXT NOT NULL,
old_value TEXT NOT NULL,
new_value TEXT NOT NULL,
notes TEXT,
user_name TEXT NOT NULL
);

REVOKE ALL ON TABLE change_log FROM edituser;
GRANT SELECT, INSERT ON TABLE change_log TO edituser;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO edituser;


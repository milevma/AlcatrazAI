# Job interview for Alcatraz AI
The goal was to do the following tasks:
1. Containerize the following App: https://github.com/priyank-purohit/PostGUI
2. Provide a Helm Chart/Templates that deploys the App above inside K8S cluster so it is in a fully working state with its required dependencies. 
3. Describe a way to automate the CI/CD process of all of the above  
(Optional for Bonus points)  => Developing a pipeline for automating all of the above in a tool of personal preference
4. BASH/GoLang: 
Obtain the POD status (from 1) and check if it is “Running”. If so 5 times in a row with 5 seconds between each check print “Successfully started” , otherwise log a message “Failed to start”. 

## Containerize the following App: https://github.com/priyank-purohit/PostGUI
The app has been build using multistage build using Dockerfile:
```bash
FROM node:14.9.0 AS build-step

WORKDIR /build
COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm run build

FROM nginx:1.18-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY example.* /etc/nginx/
COPY --from=build-step /build/build /frontend/build
```
then the docker image is uploaded to my dockerhub account using the command:
```bash
docker push milevmartin/postgui:v3
```
Next I have created a posgreSQL contained that contains the scripts/*.sql files:

```bash
FROM postgres:14.4-alpine
COPY bootstrap.sql /docker-entrypoint-initdb.d
```
```bash
docker push milevmartin/postgresql:v1
```
now when the container starts it executes the scripts.

All the other images that I have used are public.


## Helm charts

Helm charts are located in Helm directory and can be installed with the following command:
```bash
helm install postgui ./helm/ --atomic
```
The postgui should be reach by example.com host since it is hardcoded in the igress definition.
If you are using minikube you might have to edit your host file.

The postgrest should be reach by "postgrest" host only. No domain here. 
## CI/CD

For CI/CD we need to first build the 2 custom docker images:
```bash
docker build -t milevmartin/postgui:v3 .
```

```bash
cd postgresql/
docker build -t milevmartin/postgresql:v1 .
```

Next to Docker login:
```bash
docker login -u milevmartin
```

Next to docker push:
```bash
docker push milevmartin/postgui:v3 
docker push milevmartin/postgresql:v1 
```

After that we can run helm install command to deploy the app:

```bash
helm install postgui ./helm/ --atomic
```

## Bash Script

The bash script is located in script/ directory.
it is very simple script:
```bash
#!/bin/bash

c=0
while true
do
    if kubectl get pods | grep -i postgui* | grep -i running > /dev/null; then
     c=$((c+1))
     sleep 5
        if [[ "$c" == 5 ]]; then
           echo "Successfully started."
           c=0
        fi
    else
        c=0
        echo "Failed to start."
        sleep 5
    fi
done

```

#!/bin/bash

c=0
while true
do
    if kubectl get pods | grep -i postgui* | grep -i running > /dev/null; then
     c=$((c+1))
     sleep 5
        if [[ "$c" == 5 ]]; then
           echo "Successfully started."
           c=0
        fi
    else
        c=0
        echo "Failed to start."
        sleep 5
    fi
done
